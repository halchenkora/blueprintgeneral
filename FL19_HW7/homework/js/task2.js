let reset = function(){
  prize = 0;
  guessesLeft = guesses;
  max = limit;
  randomNum = Math.floor(Math.random() * max + 1);
  priceList = {
    3: 100,
    2: 50,
    1: 25
  };
  }
  let prize = 0;
  let guesses = 3;
  let guessesLeft = 3;
  let limit = 8;
  let max = 8;
  let increase = 4;
  let currentTotal;
  let attempt;
  let randomNum = Math.floor(Math.random() * max + 1);
  let priceList = {
    1: 25,
    2: 50,
    3: 100
  };
  
  (function(){
    if (confirm('Do you want to play a game?')) {
      while (confirm) {
        const result = gameStart();
        switch (result) {
          case 'SUCCESS': {
            if (confirm('Do you want to continue?')) {
              break;
            }
    
            alert(`Thank you for your participation. Your 
          prize is: ${prize}$`);
          if(confirm('Do you want to play a game?')){
            reset();
            break;
          } else {
            return;
          }
          } 
          case 'FAILURE': {
            if(confirm('Do you want to play a game?')){
              reset();
              break;
            } else {
              return;
            }
          }
          default: {
            // return empty
          }
        }
      }
    } else {
      alert('You did not become a billionaire, but can');
    }
  })()
  
  function gameStart(){  
    for(let i = guessesLeft; i > 0; i--){
      currentTotal = priceList[i];
      attempt = parseInt(
        prompt(`Choose a roulette pocket number from 0 to ${max}
Attempts left: ${i}
Total prize: ${prize}
Possible prize on current attempt:${currentTotal}$ \n`)
      );
      if (attempt === randomNum) {
        alert('Congratulations! You won!');
        max += increase;
        randomNum = Math.floor(Math.random() * max + 1);
        prize += currentTotal;
        for (let priceItem in priceList) {
          if (priceList.hasOwnProperty(priceItem)) {
            priceList[priceItem] *= 2;
          }
        }
        return 'SUCCESS'
      } else {
        if (i === 1){
          prize = 0;
          alert(`Thank you for your participation. Your 
prize is: ${prize}$`);
          return 'FAILURE'
        }
      }
    }
  }
  