// #1

function extractCurrencyValue(param) {
    const bigIntLimit = 16;
    param = param.split(' ').shift();
    if(param.length < bigIntLimit){
        return parseInt(param)
    } else {   
        return BigInt(param)
    }
}
console.log(extractCurrencyValue('120 USD')); // 120
console.log(extractCurrencyValue('1283948234720742 EUR')); // 1283948234720742n


// #2

let object = {
    name: 'Ann',
    age: 16,
    hobbies: undefined,
    degree: null,
    isChild: false,
    isTeen: true,
    isAdult: false
}

function clearObject(obj) { 
    for(let keyProperty in obj) {
        if(obj[keyProperty] === false || obj[keyProperty] === undefined || obj[keyProperty] === null){
          delete obj[keyProperty];
        }       
    }
    return obj;
}

console.log(clearObject(object)); // { name: 'Ann', age: 16, isTeen: true }


// #3

function getUnique(param) {
    return Symbol(param)
} 

console.log(getUnique('Test')) // Symbol('Test')


// #4

function countBetweenTwoDays(startDate, endDate) {
let unix = 1000;
let minutes = 3600;
let hours = 24;
let week = 7;
let month = 30;
let date1 = new Date(startDate);
let date2 = new Date(endDate);
let dateDiff = date1.getTime() - date2.getTime();
let days = Math.round(dateDiff / (unix * minutes * hours));
let weeks = Math.ceil(dateDiff / (unix * minutes * hours * week));
let months = Math.ceil(dateDiff / (unix * minutes * hours * month));
return `The difference between dates is: ${days} day(-s), ${weeks} week(-s), ${months} 
month(-s)
`
}

console.log(countBetweenTwoDays('03/22/22', '05/25/22')); // The difference between dates is: 64 day(-s), 9 week(-s), 2 month(-s)

// #5

function filterArray(arr) {
    return arr.filter((arrEl, arrNum, arrDef) => arrDef.indexOf(arrEl) === arrNum)
}
function filterArrayWithSet(arr){
    return [...new Set(arr)]
}
// console.log(filterArray([1, 2, 2, 4, 5, 5, 5, 6, 6, 7, 7, 8, 8, 8, 9])); // [1, 2, 3, 4, 5, 6, 7, 8, 9]
// console.log(filterArrayWithSet([1, 2, 2, 4, 5, 5, 5, 6, 6, 7, 7, 8, 8, 8, 9])); // [1, 2, 3, 4, 5, 6, 7, 8, 9]
