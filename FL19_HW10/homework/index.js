// Timing variables
let gameLength = 5000;
// Utility variables
let zeroValue = 0;
let emptyValue = '';
let gameResultsData, getPlayerName;
// ID's variables
let nickname = document.getElementById('nickname').value;
let start = document.getElementById('start');
let result = document.getElementById('result');
let bestResult = document.getElementById('best-result');
let clearResult = document.getElementById('clear-result');
let clearAll = document.getElementById('clear-all');
let clicker = document.getElementById('clicker');

// Defining game details
let temporaryResult = localStorage.getItem('gameResultsData');
if(temporaryResult === false){
  gameResultsData = JSON.parse(temporaryResult);
} else {
  gameResultsData = new Object();
}

// Getting new player nickname
let playerNicknameValidation = function (){
  getPlayerName = document.querySelector('#nickname').value;
}

// Setting data to local storage
let settingData = function(){
  localStorage.setItem('gameResultsData', JSON.stringify(gameResultsData));
}

// Event listeners
start.addEventListener('click', function(){
  playerNicknameValidation();
  if(getPlayerName === emptyValue){
    try { 
      if(nickname === emptyValue) { 
        throw 'Empty nickname';
      }
    } catch(error){ 
      alert(error);
    }
  } else {
    let timesBtnClicked = zeroValue;
    let btnClickedTotal = function() {
      timesBtnClicked += 1; 
      }
    clicker.addEventListener('click', btnClickedTotal)    
    setTimeout(function(){
      clicker.removeEventListener('click', btnClickedTotal);
      alert(`You clicked ${timesBtnClicked} times`);
      if(gameResultsData[getPlayerName] > timesBtnClicked){
        gameResultsData[getPlayerName]
      } else {
        gameResultsData[getPlayerName] = timesBtnClicked
      }
      settingData()
    }, gameLength)
  }
})

result.addEventListener('click', function(){
  playerNicknameValidation()
  alert(`Best result is: ${gameResultsData[getPlayerName]}`);
})

bestResult.addEventListener('click', function(){
  let bestPlayerName, bestPlayerScore = zeroValue;
  for(let playerName in gameResultsData){
    if(gameResultsData.hasOwnProperty(playerName)){
      if(bestPlayerScore < gameResultsData[playerName]){
        bestPlayerScore = gameResultsData[playerName];
        bestPlayerName = playerName;
      }
    }
  }
  alert(`Best result for the whole time is ${bestPlayerName} by ${bestPlayerScore}`);
})

clearResult.addEventListener('click', function(){
  playerNicknameValidation();
  delete gameResultsData[getPlayerName];
  settingData();
})

clearAll.addEventListener('click', function(){
  localStorage.removeItem('gameResultsData');
  gameResultsData = new Object();
})
