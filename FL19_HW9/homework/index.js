// #1
function calculateSum(arr) {
  return arr.reduce((acc, val) => acc + val, 0);
}

// console.log(calculateSum([1,2,3,4,5])); //15

// #2
function isTriangle(a, b, c) {
  if(a + b > c && a + c > b && b + c > a && a * b * c > 0){
    return true;
  } else {
    return false;
  }
}

// console.log(isTriangle(5,6,7)); //true
// console.log(isTriangle(2,9,3)); //false

// #3
function isIsogram(word) {
  let arr1 = word.toLowerCase().split('');
  let arr2 = [... new Set(arr1)];
  return arr1.length === arr2.length
}

console.log(isIsogram('Dermatoglyphics')); //true
console.log(isIsogram('abab')); //false


// #4
function isPalindrome(word) {
  return word === word.split('').reverse().join('');
}

console.log(isPalindrome('Dermatoglyphics')); //false
console.log(isPalindrome('abbabba')); //true

// #5
function showFormattedDate(dateObj) {
  let day = dateObj.getDate();
  let month = dateObj.toLocaleString('en-EN', { month: 'long' });
  let year = dateObj.getFullYear();
  return `${day} of ${month}, ${year}`
}

console.log(showFormattedDate(new Date('05/12/22'))); //'12 of May, 2022'


// #6
const letterCount = (str, letter) => {
let arr = str.split('');
let sum = 0;
for(let i = 0; i < arr.length; i++){
  if(arr[i] === letter){
    sum += 1;
  }
} return sum;
}

console.log(letterCount('abbaba', 'b')); //3

// #7
function countRepetitions(arr) {
  return arr.reduce(function(prev, cur) {
    prev[cur] = (prev[cur] || 0) + 1;
    return prev;
  }, {});
  // return arr.reduce((item, times) => (item[times] = item[times] + 1 || 1, item), {});
}

console.log(countRepetitions(['banana', 'apple', 'banana'])); // { banana: 2, apple: 1 }

// #8
function calculateNumber(arr) {
  let arrToString = arr.join('');
  return parseInt(arrToString, 2)
}

console.log(calculateNumber([0, 1, 0, 1])); //5
console.log(calculateNumber([1, 0, 0, 1])); //9
