//  #1
function getWeekDay(obj){
  let daysOfTheWeek = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
  return daysOfTheWeek[obj.getDay()];
}

console.log(getWeekDay(new Date()))
// eslint-disable-next-line no-magic-numbers
console.log(getWeekDay(new Date(2022, 2, 25)))

// #2
function getAmountDaysToNewYear(){
  let currentDay = new Date();
  let newYearDate = new Date('01/01/2023');
  let unix = 1000;
  let hours = 24;
  let minutes = 60;
  let seconds = 60;
  let timeInSeconds = unix * hours * minutes * seconds;
  return Math.floor(
    (newYearDate.getTime() - currentDay.getTime()) /
    timeInSeconds
  );
}
console.log(getAmountDaysToNewYear());

// #3
// eslint-disable-next-line no-magic-numbers
const birthday17 = new Date(2004, 12, 29);
// eslint-disable-next-line no-magic-numbers
const birthday15 = new Date(2006, 12, 29);
// eslint-disable-next-line no-magic-numbers
const birthday22 = new Date(2000, 9, 22);

function getApproveToPass(currentAge){
  let playerYear = currentAge.getFullYear();
  let currentYear = new Date().getFullYear()
  let limit = 18;
  let answer;
  let playerAge = currentYear - playerYear
  if(playerAge >= limit){
    answer = `Hello adventurer, you may pass!`
  } else if (playerAge < limit && playerAge >= limit - 1 ){
    answer = `Hello adventurer, you are to yang for this quest wait for few more months!`
  } else if(playerAge < limit - 1) {
    answer = `Hello adventurer, you are to yang for this quest wait for ${limit - playerAge} years more!` 
  }
  return answer;
}

console.log(getApproveToPass(birthday17))
console.log(getApproveToPass(birthday15))
console.log(getApproveToPass(birthday22))

// #4 
const elementP = 'tag="p" class="text" style={color: #aeaeae;} value="Aloha!"'

function stringToHTML(stringValue) {
  let tag = stringValue.match(/tag="(.*)" c/)[1];
  let className = stringValue.match(/class="(.*)" s/)[1];
  let style = stringValue.match(/style={(.*)}/)[1];
  let value = stringValue.match(/value="(.*)"/)[1];
  let finishedLine = `<${tag} class=”${className}” style=”${style}”>${value}</${tag}>`;
  return finishedLine
}

console.log(stringToHTML(elementP))

// #5
function isValidIdentifier(str) {
  let isChar = str.match(!/^.*[a-zA-Z]+.*$/gm);
  let isSymbol = str.match(/^(?!.*([$_])).*/gm)
  let isNum = str.match(/^[(1-9)0-9]/gm);
  return !isChar && !isSymbol && !isNum;
}
console.log(isValidIdentifier('myVar!')); 
console.log(isValidIdentifier('myVar$')); 
console.log(isValidIdentifier('myVar_1')); 
console.log(isValidIdentifier('1_myVar')); 

// #6
const testStr = 'My name is John Smith. I am 27.';
function transformStringToHtml(testStr){
let subst = /(\b[a-z](?!\s))/g;
return testStr.replace(subst, (string) => 
  string.toUpperCase()
);
}
console.log(transformStringToHtml(testStr))

// 7
function isValidPassword(str) {
  let subsSymbols = /^[A-Z]{1,}[a-z]{1,}[\d]{1,}$/gm;
  let atLeastLength = 8;
  let answer;
  if(subsSymbols.test(str) && str.length >= atLeastLength){
    answer = true;
  } else {
    answer = false;
  }
  return answer;
}

console.log(isValidPassword('agent007')); 
console.log(isValidPassword('AGENT007')); 
console.log(isValidPassword('AgentOOO')); 
console.log(isValidPassword('Age_007')); 
console.log(isValidPassword('Agent007')); 

// #8
function bubbleSort(array) {
    for(let b=0;b<array.length;b++) {
      for(let a = 0; a < array.length; a++) {
        if(array[a]>array[a+1]) {
          let temp = array[a];
          array[a] = array[a+1];
          array[a+1] = temp;
        }
      }
    }      
return array;
}
// eslint-disable-next-line no-magic-numbers
console.log(bubbleSort([7,5,2,4,3,9]));

// 9
const inventory = [
{ name: 'milk', brand: 'happyCow', price: 2.1 },
{ name: 'chocolate', brand: 'milka', price: 3 },
{ name: 'beer', brand: 'hineken', price: 2.2 },
{ name: 'soda', brand: 'coca-cola', price: 1 }
];
function sortByItem({ item, array }) {
  if (item === 'name' || item === 'brand') {
    array.sort((first, second) => {
      if (first[item] < second[item]) {
        // eslint-disable-next-line no-magic-numbers
          return -1;
      } else if (first[item] > second[item]) {
          return 1;
      } else {
          return 0;
      }
  });
} 
let sortedArr = array.sort((first, second) => parseFloat(first[item]) - parseFloat(second[item]));
return sortedArr;
}
console.log(sortByItem({item: 'name', array: inventory})); 





