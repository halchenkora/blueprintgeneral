// CURRENT TASK ISN'T DONE PROPERLY, IT DOESN'T WORK AS IT SHOULD
// I WASN'T ABLE TO FINISH IT BUT DECIDED TO PUSH AMOUNT OF JOB THAT WAS DONE
import { dictionary } from './dictionary.js';
const guessesLeft = 6;
let triesLen = 5;
let guessesRemaining = guessesLeft;
let guessedString = dictionary[Math.floor(Math.random() * dictionary.length)];
let currInd = 0;
let correctGuess = '';


function startPlay() {
    let board = document.getElementById('table-grid');

    for (let i = 0; i < guessesLeft; i++) {
        let row = document.createElement('div');
        row.className = 'letter-row';
        
        for (let j = 0; j < triesLen; j++) {
            let box = document.createElement('input');
            box.className = 'letter-box';
            box.maxLength = 1;
            box['index-row'] = i;
            box['index-col'] = j;
            box.addEventListener('input', onLetterChange);
            row.appendChild(box);
        }

        board.appendChild(row);
    }
}


startPlay();

function onLetterChange(event) {
    const defVal = event.target.value;
    
    if (
        Number(event.target['index-row']) !== currInd ||
        Number(event.target['index-col']) !== correctGuess.length
    ) {
        return;
    }

    if (defVal.match(/^[A-Za-z]+$/)) {
        correctGuess += defVal;
    }

    if (correctGuess.length < triesLen) {
        return;
    }

    if (!dictionary.includes(correctGuess.toLowerCase)) {
        alert('Word not in list!');
        return;
    }    

    if (correctGuess === guessedString) {
        alert('You guessed right! Game over!')
        guessesRemaining = 0;
        return;
    } else {
        guessesRemaining -= 1;
        correctGuess = [];

        if (guessesRemaining === 0) {
            alert("You've run out of guesses! Game over!");
            alert(`The right word was: "${guessedString}"`);
        }
    }
}
